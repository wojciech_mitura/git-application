import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ITaskCategory } from '../../models'
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-task-category',
  templateUrl: './task-category.component.html',
  styleUrls: ['./task-category.component.scss']
})
export class TaskCategoryComponent implements OnInit {
  @Input() taskCategory: ITaskCategory
  @Input() isActive: Boolean
  @Output() changeTasksCategoryEmitter: EventEmitter<any> = new EventEmitter();

  constructor(private tasksService: TasksService) { }

  ngOnInit(): void {}

  setCategory(): void {
    this.tasksService.setActiveCategory(this.taskCategory.title)
    this.changeTasksCategoryEmitter.emit()
  }
}
