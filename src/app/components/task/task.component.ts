import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ITask } from 'src/app/models';
import { taskCategories } from 'src/app/dictionaries/task-categories'

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  @Input() task: ITask
  @Output() changeTaskCategoryEmitter: EventEmitter<any> = new EventEmitter();
  
  public taskCategories = taskCategories

  constructor() { }

  ngOnInit(): void {
  }

  changeCategory() { 
    this.task.category = 
      this.task.category == taskCategories.OPEN ?
      'Closed' :
      'Open' // TODO , fix type - below

      // this.task.category = 
        //   this.task.category == taskCategories.OPEN ?
        //   taskCategories.CLOSED :
        //   taskCategories.OPEN
    
    this.changeTaskCategoryEmitter.emit()
  }

}
