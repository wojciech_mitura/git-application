import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import localePL from '@angular/common/locales/pl';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskCategoryComponent } from './components/task-category/task-category.component';
import { TaskComponent } from './components/task/task.component';
import { TasksService } from './services/tasks.service';
import { DatePipe, registerLocaleData } from '@angular/common';

registerLocaleData(localePL);

@NgModule({
  declarations: [
    AppComponent,
    TaskCategoryComponent,
    TaskComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    TasksService,
    DatePipe,
    { provide: LOCALE_ID, useValue: "pl" },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
