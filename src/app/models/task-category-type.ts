export type TaskCategoryType = 
  'All' |
  'Open' |
  'Closed'