import { TaskCategoryType } from './task-category-type'

export interface ITask {
    title: string,
    category: TaskCategoryType,
    date: Date
}