export interface ITaskCategory {
    title: string
    amount: number
    imageUrl: string
}