import { Component, OnInit } from '@angular/core';
import { ITask, ITaskCategory } from './models';
import { taskCategories } from './dictionaries/task-categories';
import { TasksService } from './services/tasks.service';
import { DatePipe } from '@angular/common';
import { MTasks } from './mocks/Mtasks'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public tasks: ITask[]
  public taskCategories: ITaskCategory[] 
  public activeCategory: string
  public taskDates: string[]
  public activeDates: any[]

  constructor(
    private tasksService: TasksService, 
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.tasks = MTasks

    this.tasksService.getActiveCategory().subscribe(activeCategory => {
      this.activeCategory = activeCategory
    })
    
    this.getTaskDates()
    this.updateCategoriesAmounts();
  }

  getTaskDates() {
    let taskDatesSet = new Set<string>()
    this.tasks.map(taskDate => { 
      const formatedDate: any = this.datePipe.transform(taskDate.date, 'MM.dd.yyyy')
      taskDatesSet.add(formatedDate)
    })

    this.taskDates = [...taskDatesSet]
  }

  getAmountOfCertainCategoryTasks(taskCategory: string) {
    let amount = 0;

    this.tasks.map(task => {
      if (taskCategory === task.category) {
        ++amount
      }
    })

    return amount
  }

  updateCategoriesAmounts(): void {
    const closedTasksAmount = this.getAmountOfCertainCategoryTasks(taskCategories.CLOSED)
    const openTasksAmount = this.getAmountOfCertainCategoryTasks(taskCategories.OPEN)

    this.taskCategories = [
      {
        title: taskCategories.ALL,
        amount: this.tasks.length,
        imageUrl: "/app/assets/icons/icon-github.svg"
      },
      {
        title: taskCategories.OPEN,
        amount: openTasksAmount,
        imageUrl: "/app/assets/icons/icon-open-issue.svg"
      },
      {
        title: taskCategories.CLOSED,
        amount: closedTasksAmount,
        imageUrl: "/app/assets/icons/icon-closed-issue.svg"
      }
    ]

    this.updateActiveTasks()
  }

  updateActiveTasks() {
    let activeTasks = this.tasks.filter(task => task.category === this.activeCategory)
    const activeDatesSet = new Set()

    activeTasks.map(activeTask => {
      const formatedDate: any = this.datePipe.transform(activeTask.date, 'MM.dd.yyyy')
      activeDatesSet.add(formatedDate)
    })
    this.activeDates = [...activeDatesSet]
  }
}
