import { ITask } from '../models';

export const MTasks: ITask[] = [
  {
    title: "Page changes",
    category: 'Open',
    date: new Date('08-02-20')
  },
  {
    title: "Review of last issues",
    category: 'Closed',
    date: new Date('08-02-20')
  },
  {
    title: "Sidebar changes",
    category: 'Open',
    date: new Date('08-02-20')
  },
  {
    title: "Crash issue",
    category: 'Open',
    date: new Date('08-03-20')
  },
  {
    title: "Visual update & Crash resolve",
    category: 'Open',
    date: new Date('08-04-20')
  },
  {
    title: "Crash update",
    category: 'Closed',
    date: new Date('08-05-20')
  },
  {
    title: "Footer update",
    category: 'Closed',
    date: new Date('08-05-20')
  },
  {
    title: "Navbar changes",
    category: 'Open',
    date: new Date('08-06-20')
  },
  {
    title: "Footer changes",
    category: 'Open',
    date: new Date('08-06-20')
  }
]