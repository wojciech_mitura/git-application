export const taskCategories = {
    ALL: 'All',
    OPEN: 'Open',
    CLOSED: 'Closed'
}