import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';

@Injectable()
export class TasksService {
  activeCategory$: Subject<string> = new BehaviorSubject("All")

  constructor() { }

  getActiveCategory(): Observable<string> {
    return this.activeCategory$.asObservable()
  }

  setActiveCategory(category: string): void {
    this.activeCategory$.next(category)
  }
}
